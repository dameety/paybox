<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->integer('price');
            $table->string('redirect_to');
            // $table->text('inputs')->nullable();
            $table->json('inputs')->nullable();
            $table->string('img_url')->nullable();
            $table->text('product_info')->nullable();
            $table->string('product_name')->unique();
            $table->string('brand_color')->nullable();
            $table->string('checkout_link')->nullable();
            $table->string('status')->default('published');
            $table->integer('views')->default(0)->nullable();
            $table->integer('total_sale')->default(0)->nullable(); //revenue
            $table->integer('quantity_sold')->default(0)->nullable(); //sales
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
