# Paybox
This package provides a fast setup for you to accept one off charges in your laravel applications. Only one off charges is covered, subscription isn't.


## Installation

```
    composer install dameety/Paybox
    php artisan migrate
    php artisan vendor:publish --provider="Dameety\Paybox\PayboxServiceProvider" --tag="assets"
    php artisan vendor:publish --provider="Dameety\Paybox\PayboxServiceProvider" --tag="config"
```

## Usage

The package uses the concept of pages.
- First decide a set of input fields you will often need to use in config
- Create a page that holds all your product information and select the inputs you want.
- Copy the link to the checkout page and link to it from wherever you want.
- The checkout page for the product is meant for both registered and unregistered users
- You can activate or disable the checkout page for a product
- You can also view the data for the inputs you specified for the checkout.


## Successful charge
When creating a page, you will specify a redirect route name, users will be redirected to this route after making a successful payment. In the controller method that handles request from this route you can access the transanction record created from the session. The code below shows a sample of what you could do after the user pays. It is assumed that the route name provided is 'afterPayment'


```
    Route::get('/after/payment', 'HomeController@afterpayment')->name('afterpayment');

    public function afterpayment()
    {
        $data = session('Paybox-data');

        //charge id from stripe for this payment
        $charge = $data->charge_id;

        // contains price, name, with present total_sale, quantity_sold of the product.
        $page = unserialize($data->page);

        //contains all the data provided by user at checkout excluding the card detials.
        $inptus = unserialize($data->inputs);

        //will be 'Guest' or present authenticated user email
        $user = $data->user;

        //redirect user to where you want.
    }
```

## How exceptions are handled
When an event is thrown. the details of the exception will be logged and an event will be fired with details of the excpetion so you can handle it as you wish. Each event that can be fired corresponds to a particular stripe error.

| Stripe Exception | Event fired |
| --- | --- |
| \Stripe\Error\Card::class,     | Dameety\Paybox\Events\Errors\Card |
| \Stripe\Error\RateLimit::class, | Dameety\Paybox\Events\Errors\RateLimit |
| \Stripe\Error\ApiConnection::class, | Dameety\Paybox\Events\Errors\ApiConnection |
| \Stripe\Error\InvalidRequest::class, | Dameety\Paybox\Events\Errors\InvalidRequest |
| \Stripe\Error\Authentication::class, | Dameety\Paybox\Events\Errors\Authentication |


**Example of listening to any of these events.**
```
    //In your EventServiceProvider.php
    'Dameety\Paybox\Events\Errors\Card' => [
        'App\Listeners\CardListener',
    ],
```

```
    //CardListener.php
    use Dameety\Paybox\Events\Errors\Card;

    //In the handler method
    public function handle(Card $card)
    {
        dd($card); you have access to the whole error event object.
    }
```

## Security
If you discover any security related issues, please send me an email instead of using the issue tracker.

## License
The MIT License (MIT). Please see [License File](LICENSE.md) for more information.