(function() {
	var stripe = Stripe('pk_test_GZYfG6M52r52tEHYj4G5mUkz');
  	var elements = stripe.elements();
  	var card = elements.create('card', {
   		style: {
			base: {
				iconColor: '#666EE8',
				color: '#31325F',
				fontWeight: 300,
				fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
				fontSize: '15px',
				'::placeholder': {
					color: '#CFD7E0',
				},
			},
    	}
	});

  	card.mount('#card-element');

  	function setOutcome(result) {
		var errorElement = document.querySelector('.error-toggle');
		errorElement.classList.remove('visible');

		if (result.token) {
			var form = document.querySelector('form');
			form.querySelector('input[name=stripe_token]').value = result.token.id;
			form.submit();
		} else if (result.error) {
			errorElement.textContent = result.error.message;
			errorElement.classList.add('visible');
		}
	}

  	card.on('change', function(event) {
		setOutcome(event);
  	});

	document.querySelector('form').addEventListener('submit', function(e) {
		e.preventDefault();
      	stripe.createToken(card).then(setOutcome);
	});

})();