<?php

return [


    //put an option of allowing user to fetch plan details from stripe and
    // cache it or store it in db after successful subscrpton

    /**
     * Base route to access the dashboard
     * cannot be empty
     */
    'uri' => 'paybox',

    'stripe' => env('STRIPE_SECRET'),

    /**
     * middleware that will be used
     */
    'middleware' => ['web', 'auth'],

    'stripe' => [
        'key'=> env('STRIPE_KEY')
    ],

    /**
     * When a checkout is disabled,
     * users will be redirect to this route
     */
    'unpublished' => 'home',

    'currency' => [
        'code' => 'usd',
        'symbol' => '$'
    ],

    /**
     * all product images will be stored here
     */
    'uploads' => 'vendor/paybox/uploads',

    /**
     * This are the inputs you will choose from
     * when creating a product page
     * the only validation rules used is required.
     */
    'inputs' => [
        'name',
        'city',
        'email',
        'state',
        'country',
        'address',
        'organization',
        'phone_number',
        'billing_address',
        'shipping_address'
    ],

    'billing_address' => [
        'name' => '',
        'address_line_1' => '',
        'address_line_2' => '',
        'city' => '',
        'state' => '',
        'country' => '',
        'zip' => '',
        'phone' => '',
        'url' => '',
        'vat' => '',
    ],

];