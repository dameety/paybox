<?php
namespace Dameety\Paybox\Tests;

use Exception;
use Stripe\Stripe;
use Dameety\Paybox\Tests\Utils\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Exceptions\Handler;
use Orchestra\Testbench\TestCase as Orchestra;
use Illuminate\Contracts\Debug\ExceptionHandler;

abstract class TestCase extends Orchestra
{
    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->disableExceptionHandling();

        $this->setUpDatabase();

        // $this->user = new User();

        $this->actingAs(
            new User()
        );

        Stripe::setApiKey(config('STRIPE_KEY'));
    }

    public function setUpDatabase()
    {
        $this->loadMigrationsFrom([
            '--database' => 'testing',
            '--realpath' => realpath(__DIR__ . '/migrations'),
        ]);
        $this->artisan('migrate', ['--database' => 'testing']);
    }

    public function teadDown()
    {
        \Mockery::close();
    }

    protected function getStripeKey()
    {
        $file = file(__DIR__ . '/../.env' );
        return $file[0];
    }

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'testing');
        $app['config']->set('app.key', 'base64:YXY0yANT037BClZVU5UePXbKiOwAmLrTTzG+QVzXv44=');
        $app['config']->set('STRIPE_KEY', $this->getStripeKey());
        $app['config']->set('paybox.route', 'paybox');
        $app['config']->set('paybox.middleware', ['web', 'auth']);
        $app['config']->set('paybox.unpublished', 'home');
        $app['config']->set('paybox.currency.code', 'usd');
        $app['config']->set('paybox.inputs', [
            'name', 'email', 'company', 'team'
        ]);
    }

    protected function getPackageProviders($app)
    {
        return [
            \Dameety\Paybox\PayboxServiceProvider::class,
            // \Laravel\Cashier\CashierServiceProvider::class,
            \Orchestra\Database\ConsoleServiceProvider::class,
            \Cviebrock\EloquentSluggable\ServiceProvider::class
        ];
    }

    protected function getPackageAliases($app)
    {
        return [
            //'Sentry'      => 'Cartalyst\Sentry\Facades\Laravel\Sentry',
            //'YourPackage' => 'YourProject\YourPackage\Facades\YourPackage',
        ];
    }

    protected function disableExceptionHandling()
    {
        $this->app->instance(
            ExceptionHandler::class, new class extends Handler
            {
                public function __construct() { }
                public function report(Exception $exception) { }
                public function render($request, Exception $exception)
                {
                    throw $exception;
                }
            }
        );
    }
}
