<?php
namespace Dameety\Paybox\Tests\Integration;

use Dameety\Paybox\Tests\TestCase;

class ViewTest extends TestCase
{
    /** @test */
    public function showDashboard()
    {
        $response = $this->get(route('paybox.dashboard'));

        $response->assertStatus(200);
    }

    /** @test */
    public function showLogs()
    {
        $response = $this->get(route('paybox.logs'));

        $response->assertStatus(200);
    }
}