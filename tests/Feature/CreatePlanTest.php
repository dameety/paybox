<?php
namespace Dameety\Paybox\Tests\Feature;

use Dameety\Paybox\Models\Plan;
use Dameety\Paybox\Tests\TestCase;

class CreatePlanTest extends TestCase
{
    /** @test */
    public function loadPlanCreatePage()
    {
        $response = $this->get(route('plan.create'));

        $response->assertViewIs('Paybox::plans.create')
            ->assertStatus(200)
            ->assertSee('New Plan');
    }

    public function testIndexPage()
    {
        $response = $this->get(route('plan.index'));

        $response->assertStatus(200);

    }

}