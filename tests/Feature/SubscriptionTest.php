<?php
namespace Dameety\Paybox\Tests\Feature;

use Dameety\Paybox\Models\Plan;
use Dameety\Paybox\Tests\TestCase;
use Dameety\Paybox\Tests\Utils\User;
use Illuminate\Support\Facades\Event;
use Dameety\Paybox\Events\SubscriptionChanged;
use Dameety\Paybox\Events\SubscriptionCreated;
use Dameety\Paybox\Events\SubscriptionResumed;
use Dameety\Paybox\Events\SubscriptionCancelled;
use Dameety\Paybox\Events\SubscriptionCardUpdated;

class SubscriptionTest extends TestCase
{
    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->user = new User();

        Event::fake();
    }

    /** @test */
    public function canCreateSubscription()
    {
        //use an already created plan's name
        //else i will have to create a new plan by talkig to
        //stripes api and that would take a lot of time.
        $plan = Plan::create([
            'name' => 'box',
            'amount' => 500000,
            'features' => 'random stuff',
            'interval' => 'month',
            'identifier' => 'boxx-01',
            'slug' => 'box'
        ]);

        $response = $this->post('ajax/user-subscription/store', [
            'planName' => $plan->name
        ]);

        Event::assertDispatched(SubscriptionCreated::class);
        $response   ->assertStatus(200)
                    ->assertJson(['success' => true]);
    }

    /** @test */
    public function canCancelSubscription()
    {
        $response = $this->actingAs($this->user)->post(
            '/ajax/cancelled-subscription/store'
        );

        Event::assertDispatched(SubscriptionCancelled::class);
        $response   ->assertStatus(200)
                    ->assertJson(['success' => true]);
    }

    /** @test */
    public function canSwapASubscription()
    {
        $newPlan = Plan::create([
            'name' => 'map',
            'amount' => 50000,
            'features' => 'just blank',
            'interval' => 'week',
            'identifier' => 'map-02',
            'slug' => 'map'
        ]);

        $this->user->isSubscribed = true;

        $response = $this->actingAs($this->user)->patch(
            'ajax/user-subscription/' . $newPlan->name . '/update'
        );

        Event::assertDispatched(SubscriptionChanged::class);
        $response   ->assertStatus(200)
                    ->assertJson(['success' => true]);
    }

    /** @test */
    public function canResumeSubscription()
    {
        $response = $this->actingAs($this->user)->delete(
            '/ajax/cancelled-subscription/destroy'
        );

        Event::assertDispatched(SubscriptionResumed::class);
        $response   ->assertStatus(200)
                    ->assertJson(['success' => true]);
    }

    /** @test */
    public function canUpdatePaymentInfo()
    {
        $data = ['stripeToken' => 'test_token'];

        $response = $this->actingAs($this->user)->post(('/ajax/payment-info/update'), $data);

        Event::assertDispatched(SubscriptionCardUpdated::class);
        $response   ->assertStatus(200)
                    ->assertJson(['success' => true]);
    }
}