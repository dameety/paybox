<?php
namespace Dameety\Paybox\Tests\Feature;

use Dameety\Paybox\Models\Page;
use Dameety\Paybox\Tests\TestCase;

class PagesCheckoutTest extends TestCase
{
    /** @test */
    public function publishedCheckoutLoads()
    {
        Page::create([
            'price' => 1000,
            'redirect_to' => 'afterpayment',
            'product_name' => 'name of product',
            'brand_color' => 'green',
            'inputs' => json_encode(['name', 'address', 'company']),
            'product_info' => 'this product is love by all',
            'product_image' => ''
        ]);

        $page = Page::first();
        $page->decode();

        $response = $this->get(route('checkout.show', ['slug' => $page->slug]));

        $response   ->assertStatus(200)
                    ->assertSee($page->price)
                    ->assertSee($page->inputs[0])
                    ->assertSee($page->inputs[1])
                    ->assertSee($page->inputs[2])
                    ->assertSee($page->product_name)
                    ->assertSee($page->product_info);
    }

}