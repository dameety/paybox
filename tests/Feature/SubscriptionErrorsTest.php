<?php
namespace Dameety\Paybox\Tests\Feature;

use Dameety\Paybox\Models\Errors;
use Dameety\Paybox\Tests\TestCase;
use Illuminate\Support\Facades\Auth;

class SubscriptionErrorsTest extends TestCase
{
    /** @test */
    public function loadIndexPage()
    {
        Errors::create([
            'action' => 'connecting to stripe',
            'user' => 1,
            'name' => 'the name of the error',
            'message' => 'the message of the error'
        ]);

        $errors = Errors::all();
        $error = $errors[0];

        $response = $this->get(route('subscription-errors.index'));

        $response   ->assertStatus(200)
                    ->assertSee($error->user)
                    ->assertSee($error->name)
                    ->assertSee($error->action)
                    ->assertSee($error->message);
    }

    /** @test */
    public function canDeleteErors()
    {
        $error = Errors::create([
            'action' => 'connecting to stripe',
            'user' => 1,
            'name' => 'the name of the error',
            'message' => 'the message of the error',
            'slug' => 'the-name-of-the-error'
        ]);

        $response = $this->delete('/ajax/error/' . $error->slug . '/delete');

        $response   ->assertStatus(200)
                    ->assertJson(['deleted' => true]);

        $error = Errors::findBySlug(($error->slug));
        $this->assertEquals($error, null);
    }
}