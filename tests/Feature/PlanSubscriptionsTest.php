<?php
namespace Dameety\Paybox\Tests\Feature;

use Dameety\Paybox\Models\Plan;
use Dameety\Paybox\Tests\TestCase;

class PlanSubscriptionsTest extends TestCase
{
    /** @test */
    public function loadIndexPage()
    {
        Plan::create([
            'amount' => 100,
            'name' => 'Easyy',
            'currency' => config('paybox.currency.code'),
            'features' => '9 users',
            'interval' => 'month',
            'identifier' => 'easy-2'
        ]);
        $plan = Plan::first();

        $response = $this->get(route('plan.subscriptions', ['slug' => $plan->slug]));
        $response   ->assertStatus(200)
                    ->assertSee($plan->name);
    }
}