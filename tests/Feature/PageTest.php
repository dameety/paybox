<?php
namespace Dameety\Paybox\Tests\Feature;

use Dameety\Paybox\Models\Page;
use Dameety\Paybox\Tests\TestCase;
use Illuminate\Support\Facades\Auth;

class PageTest extends TestCase
{
    /** @test */
    public function loadCreatePage()
    {
        $response = $this->get( route('page.create') );

        $response   ->assertStatus(200)
                    ->assertSee('new-page');
    }

    /** @test */
    public function pageIndexOpens()
    {
        Page::create([
            'price' => 1000,
            'redirect_to' => 'afterpayment',
            'product_name' => 'name of product',
            'brand_color' => 'green',
            'inputs' => json_encode(['name', 'address', 'company']),
            'product_info' => 'this product is love by all',
            'product_image' => ''
        ]);

        $pages = Page::all();
        $page = $pages[0];

        $response = $this->get(route('page.index'));

        $response->assertStatus(200)
            ->assertSee($page->product_name)
            ->assertSee($page->slug)
            ->assertSee($page->price);
    }

    /** @test */
    public function loadEditPage()
    {
        $page = Page::create([
            'price' => 1000,
            'redirect_to' => 'afterpayment',
            'product_name' => 'name of product',
            'brand_color' => 'green',
            'inputs' => json_encode(['name', 'address', 'company']),
            'product_info' => 'this product is love by all',
            'product_image' => ''
        ]);

        $response = $this->get(route('page.edit', ['slug' => $page->slug]));

        $response->assertStatus(200)
            ->assertSee('edit-page')
            ->assertSee($page->product_name);

    }

    /** @test */
    public function canGetInputsFromConfig()
    {
        $response = $this->get('/ajax/config/inputs');

        $response->assertStatus(200)
            ->assertJson(['name', 'email', 'company', 'team'])
            ->assertExactJson(['name', 'email', 'company', 'team']);
    }

    /** @test */
    public function storeNewPage()
    {
        $page = [
            'price' => 1000,
            'redirect_to' => 'afterpayment',
            'product_name' => 'name of product',
            'brand_color' => 'green',
            'page_inputs' => ['name', 'address', 'company'],
            'product_info' => 'this product is love by all',
            'product_image' => ''
        ];

        $response = $this->post(('/ajax/page/store'), $page);
        $response->assertStatus(200)
            ->assertJson(['created' => true]);

        $page = Page::first();
        $this->assertEquals($page->product_name, 'name of product');
        $this->assertEquals($page->price, 1000);
        $this->assertEquals($page->inputs, json_encode([
            'name', 'address', 'company'
        ]));
        $this->assertEquals($page->brand_color, 'green');
        $this->assertEquals(
            $page->checkout_link,
            url('/') . config('paybox.route') . '/' . $page->slug . '/checkout'
        );
        $this->assertEquals($page->img_url, '');
        $this->assertEquals($page->status, 'published');
        $this->assertEquals($page->product_info, 'this product is love by all');
        $this->assertEquals($page->redirect_to, 'afterpayment');
    }

    /** @test */
    public function updateAPage()
    {
        $oldPage = Page::create([
            'price' => 1000,
            'redirect_to' => 'afterpayment',
            'product_name' => 'name of product',
            'brand_color' => 'green',
            'inputs' => json_encode(['name', 'address', 'company']),
            'product_info' => 'this product is love by all',
            'product_image' => ''
        ]);

        $newPage = [
            'price' => 2000,
            'redirect_to' => 'after-update',
            'product_name' => 'new name of product',
            'brand_color' => 'red',
            'inputs' => ['email', 'phone'],
            'product_info' => 'updated product info',
            'product_image' => ''
        ];

        $response = $this->patch(('/ajax/page/' . $oldPage->slug . '/update'), $newPage);

        $response->assertStatus(200)
            ->assertJson(['updated' => true]);

        $page = Page::first();
        $this->assertEquals($page->product_name, 'new name of product');
        $this->assertEquals($page->price, 2000);
        $this->assertEquals($page->inputs, json_encode([
            'email', 'phone'
        ]));
        $this->assertEquals($page->brand_color, 'red');
        $this->assertEquals(
            $page->checkout_link,
            url('/') . config('paybox.route') . '/' . $page->slug . '/checkout'
        );
        $this->assertEquals($page->img_url, '');
        $this->assertEquals($page->status, 'published');
        $this->assertEquals($page->product_info, 'updated product info');
        $this->assertEquals($page->redirect_to, 'after-update');
    }

    /** @test */
    public function deleteAPage()
    {
        $page = Page::create([
            'price' => 1000,
            'redirect_to' => 'afterpayment',
            'product_name' => 'name of product',
            'brand_color' => 'green',
            'inputs' => json_encode(['name', 'address', 'company']),
            'product_info' => 'this product is love by all',
            'product_image' => ''
        ]);

        $response = $this->delete('/ajax/page/' . $page->slug . '/delete');
        $response->assertStatus(200)
            ->assertJson(['deleted' => true]);

        $page = Page::findBySlug($page->slug);
        $this->assertEquals($page, null);
    }
}