<?php
namespace Dameety\Paybox\Tests\Feature;

use Dameety\Paybox\Models\Page;
use Dameety\Paybox\Tests\TestCase;

class PageStatusTest extends TestCase
{
    /** @test */
    public function publishedAPage()
    {
        $data = Page::create([
            'price' => 1000,
            'redirect_to' => 'afterpayment',
            'product_name' => 'name of product',
            'brand_color' => 'green',
            'page_inputs' => serialize(['name', 'address', 'company']),
            'product_info' => 'this product is love by all',
            'product_image' => ''
        ]);

        $response = $this->post(('/ajax/published-page/store'), [
            'slug' => $data->slug
        ]);

        $response   ->assertStatus(200)
                    ->assertJson(['published' => true]);

        $page = Page::first();
        $this->assertEquals($page->status, 'published');
    }

    /** @test */
    public function unpublishedAPage()
    {
        $data = Page::create([
            'price' => 1000,
            'redirect_to' => 'afterpayment',
            'product_name' => 'name of product',
            'brand_color' => 'green',
            'page_inputs' => serialize(['name', 'address', 'company']),
            'product_info' => 'this product is love by all',
            'product_image' => ''
        ]);

        $response = $this->post(('/ajax/published-page/destroy'), [
            'slug' => $data->slug
        ]);

        $response   ->assertStatus(200)
                    ->assertJson(['unpublished' => true]);

        $page = Page::first();
        $this->assertEquals($page->status, 'unpublished');
    }
}
