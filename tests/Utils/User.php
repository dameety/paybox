<?php
namespace Dameety\Paybox\Tests\Utils;

use Laravel\Cashier\Billable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Billable;

    public $isSubscribed = false;

    public function __construct()
    {
        $this->subscriptions = collect();
    }

    public function subscribed($subscription = 'default', $plan = null)
    {
        return $this->isSubscribed;
    }

    public function subscription($subscription = 'default')
    {
        return new Subscription();
    }

    public function newSubscription($subscription = 'default', $plan = null)
    {
        return new Subscription();
    }

    public function updateCard($token)
    {
    }
}