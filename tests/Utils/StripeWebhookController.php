<?php
namespace Dameety\Paybox\Tests\Utils;

use Dameety\Paybox\Http\Controllers\StripeWebhookController as WebhookController;

class StripeWebhookController extends WebhookController
{
    /**
     * Get the user by Stripe id.
     *
     * @param  string  $stripeId
     * @return \Laravel\Cashier\Billable
     */
    protected function getUserByStripeId($id)
    {
        return new User();
    }
}