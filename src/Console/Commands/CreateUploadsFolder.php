<?php

namespace Dameety\Paybox\Console\Commands;

use Illuminate\Console\Command;

class CreateUploadsFolder extends Command
{
    protected $signature = 'paybox:create-folder';

    protected $description = 'Create a folder to keep all product images';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $folder  = config('paybox.uploads');

        if (!file_exists(public_path( $folder ))) {
            mkdir(public_path( $folder ), @755, true);
            $this->info('Paybox uploads directory has been created.');
        } else {
            $this->info('The specified Paybox uploads directory already exists.');
        }
    }
}