<?php

class Menu
{

    public static function isActiveRoute($route, $output = 'uk-active')
    {
        if (Route::currentRouteName() == $route) {
            return $output;
        }
    }

    public static function areActiveRoutes(Array $routes, $output = 'uk-active')
    {
        foreach ($routes as $route) {
            if (Route::currentRouteName() == $route) {
                return $output;
            }
        }
    }

}