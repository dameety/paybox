<?php

namespace Dameety\Paybox;

use Stripe\Stripe;

class StripeGateway
{
    public function __construct()
    {
        // Stripe::setApiKey(config('STRIPE_KEY'));
        Stripe::setApiKey(env('STRIPE_KEY'));
        // Stripe::setApiKey(config('services.stripe.secret'));
        // Stripe::setApiKey( config('paybox.stripe') );
        // Stripe::setApiKey('sk_test_s9PEpQNCoaxOd2HWh6TRWMzt');
    }

    public function charge($stripeToken, $page)
    {
        try {
            $charge = \Stripe\Charge::create(
                array(
                    "amount" => $page->price * 100,
                    "source" => $stripeToken,
                    "currency" => config('paybox.currency.code'),
                    "description" => 'Payment for ' . $page->product_name . ', on ' . config('app.name')
                )
            );
            return $charge->id;
        } catch (\Stripe\Error\Base $e) {
            return $e;
        }
    }

    public function refundCharge($data)
    {
        try {
            $refund = \Stripe\Refund::create(array(
                "charge" => $data['charge_id'],
                "amount" => $data['amount']
            ));
            return $refund->id;
        } catch (\Stripe\Error\Base $e) {
            return $e;
        };
    }

    public function getRefunds($data)
    {
        try {
            $refunds = \Stripe\Refund::all(array(
                "charge" => $data
            ));
            return $refunds->data;
        } catch (\Stripe\Error\Base $e) {
            return $e;
        };
    }

    public function createPlan($data)
    {
        try {
            return $plan = \Stripe\Plan::create([
                'amount' => (int)$data['amount'],
                'interval' => $data['interval'],
                'name' => $data['name'],
                "currency" => config('paybox.currency.code'),
                'id' => $data['identifier']
            ]);
        } catch (\Stripe\Error\Base $e) {
            throw $e;
        }
    }

    public function getPlan($id)
    {
        try {

            return $plan = \Stripe\Plan::retrieve($id);

        } catch (\Stripe\Error\Base $e) {
            dd($e);
        }
    }

    public function deletePlan($id)
    {
        try {
            $plan = \Stripe\Plan::retrieve($id);
            $plan->delete();
        } catch (\Stripe\Error\Base $e) {
            return $e;
        }
    }
}
