<?php

namespace Dameety\Paybox;

use Laravel\Cashier\Cashier;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Dameety\Paybox\Repositories\Logs\EloquentLog;
use Dameety\Paybox\Repositories\Logs\LogRepository;
use Dameety\Paybox\Repositories\Pages\EloquentPage;
use Dameety\Paybox\Repositories\Pages\PageRepository;
use Dameety\Paybox\Console\Commands\CreateUploadsFolder;
use Dameety\Paybox\Repositories\Transactions\EloquentTransaction;
use Dameety\Paybox\Repositories\Transactions\TransactionRepository;
use Cviebrock\EloquentSluggable\ServiceProvider as SluggableServiceProvider;

class PayboxServiceProvider extends ServiceProvider
{
    protected $defer = false;

    public function boot()
    {
        $this->configure();

        $this->registerViews();

        $this->assertPublishing();

        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');

        $this->registerRoutes();

        $this->registerHelpers();

        $this->registerCommands();

        // Cashier::useCurrency('eur', '€');

        Cashier::useCurrency(config('paybox.currency.code'), config('paybox.currency.symbol'));
    }

    public function configure()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/paybox.php', 'config');
        $this->publishes([__DIR__ . '/../config/paybox.php' => config_path('paybox.php')], 'config');
        // php artisan vendor:publish --provider="Dameety\Paybox\PayboxServiceProvider" --tag="config"
    }

    public function assertPublishing()
    {
        $this->publishes([__DIR__ . '/../public' => public_path('vendor/paybox')], 'assets');
        // php artisan vendor:publish --provider="Dameety\Paybox\PayboxServiceProvider" --tag="assets"
    }

    public function registerViews()
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'Paybox');
        $this->publishes([__DIR__ . '../resources/views' => resource_path('views/vendor/paybox')], 'views');
        // php artisan vendor : publish --provider = "Dameety\Paybox\PayboxServiceProvider" --tag = "views"
    }

    public function registerRoutes()
    {
        Route::group([
            'middleware' => config('paybox.middleware'),
            'namespace' => 'Dameety\Paybox\Http\Controllers'
        ], function () {
            $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        });
    }

    public function register()
    {
        $this->app->register(SluggableServiceProvider::class);

        $this->app->bind(LogRepository::class, EloquentLog::class);
        $this->app->bind(PageRepository::class, EloquentPage::class);
        $this->app->bind(TransactionRepository::class, EloquentTransaction::class);
    }

    public function registerCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                CreateUploadsFolder::class
            ]);
        }
    }

    public function registerHelpers()
    {
        require_once( __DIR__.'/Helpers/menu.php');
    }
}