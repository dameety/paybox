<?php

namespace Dameety\Paybox\Exceptions;

use Dameety\Paybox\Events\Errors\Card;
use Dameety\Paybox\Events\Errors\RateLimit;
use Dameety\Paybox\Events\Errors\ApiConnection;
use Dameety\Paybox\Events\Errors\InvalidRequest;
use Dameety\Paybox\Events\Errors\Authentication;
use Dameety\Paybox\Repositories\Logs\LogRepository;

class Handler
{
    protected $log;
    protected $page;
    protected $data;
    protected $exception;
    protected $exceptionClass;

    public function __construct($exception, $page, $data, LogRepository $log)
    {
        $this->log = $log;
        $this->page = $page;
        $this->data = $data;
        $this->exception = $exception;
    }

    protected $expectedExceptions = [
        \Stripe\Error\Base::class,
        \Stripe\Error\Card::class,
        \Stripe\Error\RateLimit::class,
        \Stripe\Error\ApiConnection::class,
        \Stripe\Error\InvalidRequest::class,
        \Stripe\Error\Authentication::class
    ];

    public function respond()
    {
        $this->getExceptionClass();

        $functionName = lcfirst (
            preg_replace("/[^a-zA-Z]/", "", $this->exceptionClass)
        );

        return $this->$functionName();
    }

    public function getExceptionClass()
    {
        $this->exceptionClass = get_class($this->exception);

        if ( ! in_array($this->exceptionClass, $this->expectedExceptions) ) {
            //something unrelated to stripe happened, handle it here
            // event( new UnexpectedException($page) );
        }
    }

    public function logIt()
    {
        $this->log->create(
            $this->exception,
            $this->page,
            $this->page->getCheckOutInputs(unserialize($this->page->inputs), $this->data)
        );
    }

    public function stripeErrorCard()
    {
        event(new Card($this->exception));
        $this->logit();
        return $this->exception->getMessage();
    }

    public function stripeErrorInvalidRequest()
    {
        event(new InvalidRequest($this->exception));
        $this->logit();
        return $this->exception->getMessage();
    }

    public function stripeErrorAuthentication()
    {
        event(new Authentication($this->exception));
        $this->logit();
        return 'Your payment could processed. Please try again.';
    }

    public function stripeErrorRateLimit()
    {
        event(new RateLimit($this->exception) );
        $this->logit();
        return 'Your payment could processed. Please try again.';
    }

    public function stripeErrorApiConnection()
    {
        event(new ApiConnection($this->exception) );
        $this->logit();
        return 'Your payment could processed. Please try again.';
    }

    public function stripeErrorBase()
    {
        return 'Your payment could processed. Please try again.';
    }
}