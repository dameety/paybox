<?php

namespace Dameety\Paybox\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{

    protected $fillable = [
        'page_id', 'page', 'summary', 'status', 'user', 'charge_id', 'inputs'
    ];

    public function page()
    {
        return $this->belongsTo('Dameety\Paybox\Models\Page');
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->diffForHumans();
    }

    public function getInputs(array $inputs, array $data)
    {
        $result = [];
        if ( count($inputs)) {
            for($i = 0; $i < count($inputs); $i++) {
                if (array_key_exists( $inputs[$i], $data)) {
                    $result[ $inputs[$i] ] = $data[ $inputs[$i] ];
                }
            }
            return $result;
        }
        return $result;
    }

}