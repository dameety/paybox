<?php
namespace Dameety\Paybox\Models;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $dates = [
        'created_at',
        'trial_ends_at'
    ];

    public function getEndsAtAttribute($value)
    {
        // if(!$value === null) {
            // return Carbon::parse($value)->format('m/d/Y');
        // }
    }
}