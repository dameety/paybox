<?php

namespace Dameety\Paybox\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;

class Page extends Model
{
    use Sluggable, SluggableScopeHelpers;

    protected $fillable = [
        'product_name', 'redirect_to', 'price', 'views', 'inputs', 'quantity_sold', 'brand_color', 'total_sale', 'status', 'product_info', 'checkout_link', 'slug', 'img_url'
    ];

    protected $hidden = [
        'id', 'updated_at'
    ];

    // protected $casts = [
    //     'inputs' => 'array',
    // ];

    public function decode ()
    {
        $this->inputs = json_decode($this->inputs);
        return $this;
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'product_name'
            ]
        ];
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function transactions()
    {
        return $this->hasMany('Dameety\Paybox\Models\Transaction')->latest();
    }

    public static function saveImage($imageData)
    {
        $data = base64_decode( explode(',', $imageData)[1] );

        if ( strpos($imageData, 'png') !== false ) {
            $fileName = Carbon::now()->timestamp . '_' . '_' . uniqid('', true) . '.png';
            $file = config('paybox.uploads') . '/' . $fileName;
            file_put_contents($file, $data);
            return $fileName;
        } else {
            $fileName = Carbon::now()->timestamp . '_' . '_' . uniqid('', true) . '.jpeg';
            $file = config('paybox.uploads') . '/' . $fileName;
            file_put_contents($file, $data);
            return $fileName;
        }
    }

    public function deleteImage()
    {
        unlink( $this->img_url );
    }

    /**
     * inputs from user filling the checkoutforms
     */
    public function getCheckOutInputs(array $inputs, array $data)
    {
        $result = [];
        if ( count($inputs)) {
            for($i = 0; $i < count($inputs); $i++) {
                if (array_key_exists( $inputs[$i], $data)) {
                    $result[ $inputs[$i] ] = $data[ $inputs[$i] ];
                }
            }
            return $result;
        }
        return $result;
    }

    public static function makeRules($inputs)
    {
        $rules = [];
        if ( count($inputs)) {
            for($i = 0; $i < count($inputs); $i++) {
                $rules[ $inputs[$i] ] = 'required';
            }
            return $rules;
        }
        return $rules;
    }

    public function increaseQtySoldAndTotalSale()
    {
        $this->update([
            'quantity_sold' => $this->quantity_sold + 1,
            'total_sale' => $this->total_sale + $this->price
        ]);
    }

    public function addCheckOutLink ()
    {
        $this->update([
            'checkout_link' => url('/') . config('paybox.route') . '/' . $this->slug . '/checkout'
        ]);
    }

    public function unPublish()
    {
        $this->update(['status' => 'unpublished']);
    }

    public function publish()
    {
        $this->update(['status' => 'published']);
    }

    public function increaseViews()
    {
        $this->update([
            'views' => $this->views + 1
        ]);
        return $this;
    }

}