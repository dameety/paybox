<?php
namespace Dameety\Paybox\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;

class Log extends Model
{
    use Sluggable, SluggableScopeHelpers;

    protected $fillable = [
        'subject', 'user', 'status', 'page', 'inputs', 'exception', 'slug'
    ];

    protected $hidden = [
        'id', 'updated_at'
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'subject'
            ]
        ];
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->diffForHumans();
    }

}