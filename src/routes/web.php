<?php

use Illuminate\Support\Facades\Route;

Route::prefix(config('paybox.uri'))->group(function () {

    Route::get('/', 'DashboardController@index')
        ->name('paybox.dashboard');

    //billing
    Route::get('/{email}/billing', 'UsersBillingController@show')
        ->name('user-billing.show');

    //Page
    Route::get('/pages', 'PagesController@index')->name('page.index');
    Route::get('/page/create', 'PagesController@create')
        ->name('page.create');
    Route::get('/page/{slug}/edit', 'PagesController@edit')
        ->name('page.edit');
    Route::get('/{slug}/checkout', 'PagesController@checkOut')
        ->name('page.checkout');

    //Plan
    Route::get('/plans', 'PlansController@index')->name('plan.index');
    Route::get('/plan/create', 'PlansController@create')
        ->name('plan.create');
    Route::post('/plan/store', 'PlansController@store')
        ->name('plan.store');

    Route::get('/plan/{slug}/subscriptions', 'PlanSubscriptionsController@index')
        ->name('plan.subscriptions');

    //Transactions
    Route::get('/page/{slug}/transactions', 'TransactionsController@pageTransactions')
        ->name('page.transactions');

    //Logs
    Route::get('/logs', 'LogsController@index')->name('paybox.logs');

    //Erors
    Route::get('/plans/errors', 'PlansErrorsController@index')->name('subscription-errors.index');

});

//checkout routes
Route::group([
    'middleware' => ['web'],
    'prefix' => config('paybox.uri'),
], function () {

    Route::get('/{slug}/checkout', 'PagesCheckoutController@show')
        ->name('checkout.show');
    Route::post('/payment/charge', 'PaymentsController@processCharge')
        ->name('processCharge');

});

Route::group([
    'prefix' => 'ajax',
    'namespace' => 'Ajax',
], function () {

    //Inputs
    Route::get('/config/inputs', 'InputsController@index');

    //Payment info
    Route::post('/payment-info/update', 'PaymentInfoController@update');

    //User Subscriptions
    Route::get('/user-subscription/{planName}/show', 'UserSubscriptionsController@show');
    Route::post('/user-subscription/store', 'UserSubscriptionsController@store');
    Route::patch('/user-subscription/{plan}/update', 'UserSubscriptionsController@update');

    //Cancelled subscriptions
    Route::post('/cancelled-subscription/store', 'CancelledUserSubscriptionsController@store');
    Route::get('/cancelled-subscription/show', 'CancelledUserSubscriptionsController@show');
    Route::delete('/cancelled-subscription/destroy', 'CancelledUserSubscriptionsController@destroy');
    Route::delete('/resumeandswap-subscription/{plan}/destroy', 'CancelledUserSubscriptionsController@destroy');

    //Page
    Route::post('/page/store', 'PagesController@store');
    Route::get('/page/{slug}', 'PagesController@getPage');
    Route::patch('/page/{slug}/update', 'PagesController@update');
    Route::delete('/page/{slug}/delete', 'PagesController@destroy');

    //Published pages
    Route::post('/published-page/store', 'PublishedPagesController@store');
    Route::post('/published-page/destroy', 'PublishedPagesController@destroy');

    //Logs
    Route::get('/logs', 'LogsController@index');
    Route::delete('/logs/clear', 'LogsController@clearAll');
    Route::delete('/log/{slug}/delete', 'LogsController@destroy');

    //Plan
    Route::delete('/plan/{slug}/delete', 'PlansController@destroy');

    // Transactions
    Route::delete('/transaction/{id}/delete', 'TransactionsController@delete');

    //Refund
    Route::post('/payment/refund', 'PaymentsController@makeRefund');
    Route::get('/payment/{charge_id}/refunds', 'PaymentsController@getRefunds');

    //Errors
    Route::delete('/error/{slug}/delete', 'SubscriptionErrorsController@destroy');
});