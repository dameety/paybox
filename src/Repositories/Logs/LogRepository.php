<?php

namespace Dameety\Paybox\Repositories\Logs;

interface LogRepository
{
    public function clearAll();
    public function destroy(string $slug);
    public function getBySlug(string $slug);
    public function create($exception, $page, $inputs);
}