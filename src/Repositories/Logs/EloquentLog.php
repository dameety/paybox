<?php

namespace Dameety\Paybox\Repositories\Logs;

use Dameety\Paybox\Models\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class EloquentLog implements LogRepository
{
    private $log;

    public function __construct(Log $log)
    {
        $this->log = $log;
    }

    public function all()
    {
        $logs = $this->log->latest('created_at')->simplePaginate(10);

        $logs->transform( function($log, $key) {
            $log->inputs = unserialize($log->inputs);
            $log->exception = unserialize($log->exception);
            return $log;
        });

        return $logs;
    }

    public function getBySlug(string $slug)
    {
        return $this->log->findBySlug($slug);
    }

    public function create($exception, $page, $checkOutInputs)
    {
        $this->log->create([
            'page' => $page->product_name,
            'exception' => serialize([
                'type' => $exception->getJsonBody()['error']['type'],
                'code' => $exception->getJsonBody()['error']['code'],
                'status' => $exception->getHttpStatus()
            ]),
            'inputs' => serialize($checkOutInputs),
            'status' => 'failure',
            'subject' => $exception->getMessage(),
            'ip' => request()->ip( ),
            'url' => request()->fullUrl(),
            'method' => request()->method(),
            'user' => Auth::check() ? Auth::user()->email : 'Guest'
        ]);
    }

    public function destroy(string $slug)
    {
        $this->getBySlug($slug)->delete();
    }

    public function clearAll()
    {
        DB::delete('delete from logs');
    }
}