<?php

namespace Dameety\Paybox\Repositories\Pages;

use Dameety\Paybox\Models\Page;

class EloquentPage implements PageRepository
{
    private $page;

    public function __construct(Page $page)
    {
        $this->page = $page;
    }

    public function getAll()
    {
        return $this->page->latest('created_at')->simplePaginate(9);
    }

    public function stats(string $value)
    {
        if ($value === 'revenue') {
            return $this->page->sum('total_sale');
        } elseif($value === 'sales') {
            return $this->page->sum('quantity_sold');
        } elseif($value === 'views') {
            return $this->page->sum('views');
        }
    }

    public function getBySlug(string $slug)
    {
        return $this->page->findBySlug($slug);
    }

    public function create(array $data)
    {
        $page = $this->page;
        $page->price = $data['price'];
        $page->redirect_to = $data['redirect_to'];
        $page->product_name = $data['product_name'];
        $page->brand_color = $data['brand_color'];
        $page->inputs = json_encode($data['page_inputs']);
        $page->product_info = $data['product_info'];
        $page->img_url = $data['product_image'] ? config('paybox.uploads') . '/' . $this->page->saveImage($data['product_image']) : '';
        $page->save();
        $page->addCheckOutLink();
    }

    public function update(string $slug, array $data, $product_image = '')
    {
        $page = $this->getBySlug($slug);
        if ( $product_image ) {
            $this->page->removeImage($slug);
            $page->price = $data['price'];
            $page->brand_color = $data['brand_color'];
            $page->redirect_to = $data['redirect_to'];
            $page->inputs = json_encode($data['inputs']);
            $page->product_name = $data['product_name'];
            $page->product_info = $data['product_info'];
            $page->img_url = config('paybox.uploads') . '/' . $this->page->saveImage($data['product_image']);
        } else {
            $page->price = $data['price'];
            $page->redirect_to = $data['redirect_to'];
            $page->brand_color = $data['brand_color'];
            $page->inputs = json_encode($data['inputs']);
            $page->product_name = $data['product_name'];
            $page->product_info = $data['product_info'];
        }
        $page->save();
        $page->addCheckOutLink();
    }

    public function getPageDetails(string $slug)
    {
        return $this->getBySlug($slug, true);
    }

    public function getTransactions(string $slug)
    {
        $transactions = $this->getBySlug($slug)->transactions;
        $transactions->transform( function($transaction, $key) {
            $transaction->page = unserialize($transaction->page);
            $transaction->inputs = unserialize($transaction->inputs);
            return $transaction;
        });
        return $transactions;
    }

    public function getRules(string $slug)
    {
        $inputs = $this->getBySlug($slug, true)->inputs;
        return $this->page->makeRules($inputs);
    }

    public function destroy(string $slug)
    {
        $page = $this->getBySlug($slug);
        if(! $page->img_url === null) {
            $page->deleteImage();
        }
        $page->delete();
    }
}