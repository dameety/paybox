<?php

namespace Dameety\Paybox\Repositories\Pages;

interface PageRepository
{
    public function getAll();

    public function getBySlug(string $slug);

    public function create(array $data);

    public function destroy(string $slug);

    public function getTransactions(string $slug);

    public function update(string $slug, array $request);

}