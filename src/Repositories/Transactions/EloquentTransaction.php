<?php

namespace Dameety\Paybox\Repositories\Transactions;

use Illuminate\Support\Facades\Auth;
use Dameety\Paybox\Models\Transaction;
use Dameety\Paybox\Repositories\Pages\PageRepository;

class EloquentTransaction implements TransactionRepository
{
    private $page;
    private $transaction;

    public function __construct(Transaction $transaction, PageRepository $page)
    {
        $this->page = $page;
        $this->transaction = $transaction;
    }

    public function getAll()
    {
        return $this->transaction->all();
    }

    public function getById(int $id)
    {
        return $this->transaction->find($id);
    }

    public function create(array $data, string $charge_id, $page)
    {
        return $transaction = $this->transaction->create([
            'status' => 'success',
            'summary' => 'Successful payment.',
            'user' => Auth::check() ? Auth::user()->email : 'Guest',
            'page' => serialize([
                'price' => $page->price,
                'name' => $page->product_name,
                'total_sale' => $page->total_sale,
                'quantity_sold' => $page->quantity_sold
            ]),
            'page_id' => (int)$page->id,
            'inputs' => serialize(
                $this->transaction->getInputs(unserialize($page->inputs), $data)
            ),
            'charge_id' => $charge_id
        ]);
    }

    public function delete(string $id)
    {
        return $this->getById($id)->delete();
    }

    public function totalCount()
    {
        return $this->transaction->all()->count();
    }
}