<?php

namespace Dameety\Paybox\Repositories\Transactions;

interface TransactionRepository
{
    public function getAll();

    public function create(array $data, string $charge_id, $page);

    public function delete(string $id);

    public function totalCount();
}
