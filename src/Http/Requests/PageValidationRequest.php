<?php

namespace Dameety\Paybox\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PageValidationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->isMethod('post')) {
            return [
                'redirect_to' => 'required',
                'price' => 'required|integer',
                'product_info' => 'required|max:300',
                'product_name' => 'required|string|unique:pages'
            ];
        } elseif($this->isMethod('patch')) {
            return [
                'redirect_to' => 'required',
                'price' => 'required|integer',
                'product_name' => 'required|string',
                'product_info' => 'required|max:300'
            ];
        }
    }
}
