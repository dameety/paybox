<?php
namespace Dameety\Paybox\Http\Controllers;

use Illuminate\Http\Request;
use Dameety\Paybox\Models\Plan;
use Illuminate\Support\Facades\DB;
use Dameety\Paybox\Models\Subscription;
use Dameety\Paybox\Http\Controllers\Controller;

class PlanSubscriptionsController extends Controller
{
    public function index($planSlug)
    {
        $plan = Plan::findBySlug($planSlug);
        $subs = Subscription::where('stripe_plan', $plan->identifier)->get();

        return view('Paybox::plan-subscriptions.index', [
            'plan' => $plan,
            'total' => $subs->count(),
            'subscriptions' => $subs
        ]);
    }
}