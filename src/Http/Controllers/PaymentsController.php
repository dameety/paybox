<?php

namespace Dameety\Paybox\Http\Controllers;

use Dameety\Paybox\Payment;
use Illuminate\Http\Request;
use Dameety\Paybox\Exceptions\Handler;
use Dameety\Paybox\Http\Controllers\Controller;
use Dameety\Paybox\Repositories\Logs\LogRepository;
use Dameety\Paybox\Repositories\Pages\PageRepository;
use Dameety\Paybox\Repositories\Transactions\TransactionRepository;

class PaymentsController extends Controller
{
    protected $log;
    protected $page;
    protected $payment;
    protected $transaction;

    public function __construct(
        PageRepository $page,
        Payment $payment,
        TransactionRepository $transaction,
        LogRepository $log )
    {
        $this->log = $log;
        $this->page = $page;
        $this->payment = $payment;
        $this->transaction = $transaction;
    }

    public function processCharge(Request $request)
    {
        $request->validate( $this->page->getRules($request->slug) );
        $page = $this->page->getBySlug($request->slug);
        $response = $this->payment->charge( $request->stripe_token, $page );

        if( is_object( $response ) ) {
            $message = (new Handler(
                $response, $page, $request->all(), $this->log
            ))->respond();
            return back()->with('paymentError', $message );
        }

        $page->increaseQtySoldAndTotalSale();
        $response = $this->transaction->create($request->all(), $response, $page);
        return redirect()->route( $page->redirect_to )->with('Paybox-data', $response);
    }

}