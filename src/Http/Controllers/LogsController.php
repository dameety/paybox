<?php

namespace Dameety\Paybox\Http\Controllers;

use Dameety\Paybox\Http\Controllers\Controller;
use Dameety\Paybox\Repositories\Logs\LogRepository;

class LogsController extends Controller
{
    protected $log;

    public function __construct(LogRepository $log)
    {
        $this->log = $log;
    }

    public function index()
    {
        return view('Paybox::logs.index', [
            'logs' => $this->log->all()
        ]);
    }
}