<?php
namespace Dameety\Paybox\Http\Controllers;

use Dameety\Paybox\Models\Plan;
use Dameety\Paybox\StripeGateway;
use Dameety\Paybox\Http\Requests\PlanRequest;
use Dameety\Paybox\Http\Controllers\Controller;

class PlansController extends Controller
{
    protected $stripeGateway;

    public function __construct(StripeGateway $stripeGateway)
    {
        $this->stripeGateway = $stripeGateway;
    }

    public function index()
    {
        return view('Paybox::plans.index', [
            'plans' => Plan::latest('created_at')->get()
        ]);
    }

    public function create()
    {
        return view('Paybox::plans.create');
    }

    public function store(PlanRequest $request)
    {
        $stripe = new StripeGateway;
        $stripe->createPlan( $request->all() );

        Plan::create([
            'name' => $request->name,
            'amount' => $request->amount,
            'features' => $request->features,
            'interval' => $request->interval,
            'identifier' => $request->identifier
        ]);

        return redirect()->route('plan.create')
            ->with('createPlan', 'The plan was created successfully');
    }
}
