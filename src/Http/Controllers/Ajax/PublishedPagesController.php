<?php

namespace Dameety\Paybox\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use Dameety\Paybox\Http\Controllers\Controller;
use Dameety\Paybox\Repositories\Pages\PageRepository;

class PublishedPagesController extends Controller
{
    protected $page;

    public function __construct(PageRepository $page)
    {
        $this->page = $page;
    }

    public function store(Request $request)
    {
        $page = $this->page->getBySlug($request->slug);
        $page->publish();
        return response()->json(['published' => true]);
    }

    public function destroy(Request $request)
    {
        $page = $this->page->getBySlug($request->slug);
        $page->unPublish();
        return response()->json(['unpublished' => true]);
    }
}