<?php

namespace Dameety\Paybox\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use Dameety\Paybox\Http\Controllers\Controller;
use Dameety\Paybox\Repositories\Transactions\TransactionRepository;

class TransactionsController extends Controller
{
    protected $transaction;

    public function __construct(TransactionRepository $transaction)
    {
        $this->transaction = $transaction;
    }

    public function delete($id)
    {
        $this->transaction->delete($id);
        return response()->json(['deleted' => true]);
    }
}