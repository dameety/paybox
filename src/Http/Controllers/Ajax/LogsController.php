<?php

namespace Dameety\Paybox\Http\Controllers\Ajax;

use Dameety\Paybox\Http\Controllers\Controller;
use Dameety\Paybox\Repositories\Logs\LogRepository;

class LogsController extends Controller
{
    protected $log;
    public function __construct(LogRepository $log)
    {
        $this->log = $log;
    }
    public function index()
    {
        return $this->log->paginated(10);
    }

    public function destroy(string $slug)
    {
        $this->log->destroy($slug);
        return response()->json(['deleted' => true]);
    }

    public function clearAll()
    {
        $this->log->clearAll();
        return response()->json(['cleared' => true], 200);
    }
}