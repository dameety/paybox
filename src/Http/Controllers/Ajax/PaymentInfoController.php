<?php
namespace Dameety\Paybox\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use Dameety\Paybox\Exceptions\ErrorHandler;
use Dameety\Paybox\Http\Controllers\Controller;
use Dameety\Paybox\Events\SubscriptionCardUpdated;

class PaymentInfoController extends Controller
{
    public function update(Request $request)
    {
        $user = auth()->user();

        try {
            $user->updateCard($request->stripeToken);
        } catch (\Stripe\Error\Base $e) {
            $message = (new ErrorHandler($e, 'resuming a cancelled subscription'))->respond();
            return response()->json(['error' => $message], 422);
        }

        event(new SubscriptionCardUpdated($user));

        return response()->json(['success' => true]);
    }
}