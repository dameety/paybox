<?php

namespace Dameety\Paybox\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use Dameety\Paybox\Http\Controllers\Controller;
use Dameety\Paybox\Repositories\Pages\PageRepository;
use Dameety\Paybox\Http\Requests\PageValidationRequest;

class PagesController extends Controller
{
    protected $page;

    public function __construct(PageRepository $page)
    {
        $this->page = $page;
    }

    public function totalRevenue ()
    {
        return $this->page->stats('revenue');
    }

    public function totalSales ()
    {
        return $this->page->stats('sales');
    }

    public function totalViews ()
    {
        return $this->page->stats('views');
    }

    public function store(PageValidationRequest $request)
    {
        $this->page->create( $request->all() );
        return response()->json(['created' => true]);
    }

    public function getPage(string $slug)
    {
        return $this->page->getPageDetails($slug);
    }

    public function update(string $slug, PageValidationRequest $request)
    {
        $this->page->update($slug, $request->all(), $request->product_image);
        return response()->json( ['updated' => true] );
    }

    public function destroy(string $slug)
    {
        $this->page->destroy($slug);
        return response()->json(['deleted' => true]);
    }

}