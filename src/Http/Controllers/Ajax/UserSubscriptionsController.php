<?php
namespace Dameety\Paybox\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use Dameety\Paybox\Models\Plan;
use Dameety\Paybox\Exceptions\ErrorHandler;
use Dameety\Paybox\Events\SubscriptionCreated;
use Dameety\Paybox\Events\SubscriptionChanged;
use Dameety\Paybox\Http\Controllers\Controller;

class UserSubscriptionsController extends Controller
{
    public function show(string $planName)
    {
        $response = [
            'hasSubscription' => false,
            'planSubscribedTo' => '',
            'isCancelled' => false
        ];

        $user = auth()->user();

        if ($user->subscribed('main')) {
            $response['hasSubscription'] = true;
        }
        $plan = Plan::where('name', $planName)->first();

        if ($user->subscribedToPlan($plan->identifier, 'main')) {
            $response['planSubscribedTo'] = $planName;
        }

        if ($user->subscribed('main')) {
            if ($user->subscription('main')->onGracePeriod()) {
                $response['isCancelled'] = true;
            }
        } else {
            $response['isCancelled'] = false;
        }

        return response()->json($response);
    }

    /**
     * Creating a new subscription
     *
     * @param Request $request
     */
    public function store(Request $request)
    {
        $plan = Plan::where('name', $request->planName)->first();
        if(!$plan) {
            return response()->json(['message' => 'Could not find plan with name'], 404);
        }
        $user = $request->user();

        $subscription = $user->newSubscription('main', $plan->identifier);

        try {
            $subscription->create($request->stripeToken, [
                'email' => $user->email,
            ]);
        } catch (\Stripe\Error\Base $e) {
            $message = (new ErrorHandler($e, 'resuming a cancelled subscription'))->respond();
            return response()->json(['error' => $message], 422);
        }

        event(new SubscriptionCreated($user, $plan->identifier));

        return response()->json(['success' => true]);
    }

    /**
     * Swapping a user's subscription
     *
     * @param Request $request
     * @param [string] $planName
     */
    public function update(Request $request, $planName)
    {
        $plan = Plan::where('name', $planName)->first();
        if (!$plan) {
            return response()->json(['message' => 'Could not find plan.'], 404);
        }

        $user = $request->user();

        try {
            $user->subscription('main')->swap( $plan->identifier );
        } catch (\Stripe\Error\Base $e) {
            $message = (new ErrorHandler($e, 'resuming a cancelled subscription'))->respond();
            return response()->json(['error' => $message], 422);
        }

        event(new SubscriptionChanged($user, $plan->identifier));

        return response()->json(['success' => true]);
    }
}