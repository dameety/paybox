<?php
namespace Dameety\Paybox\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use Dameety\Paybox\Models\Plan;
use Dameety\Paybox\Exceptions\Handler;
use Dameety\Paybox\Exceptions\ErrorHandler;
use Dameety\Paybox\Events\SubscriptionResumed;
use Dameety\Paybox\Http\Controllers\Controller;
use Dameety\Paybox\Events\SubscriptionCancelled;

class CancelledUserSubscriptionsController extends Controller
{
    /**
     * Cancel a subscription
     */
    public function store(Request $request)
    {
        $user = auth()->user();

        try {
            $user->subscription('main')->cancel();
        } catch (\Stripe\Error\Base $e) {
            $message = (new ErrorHandler($e, 'cacelling a subscription'))->respond();
            return response()->json(['error' => $message], 422);
        }

        event(new SubscriptionCancelled($user));

        return response()->json(['success' => true]);
    }

    public function show()
    {
        $user = auth()->user();

        $response = [
            'isCancelled' => false,
            'hasSubscription' => false
        ];

        if ( $user->subscribed('main') ) {
            $response['hasSubscription'] = true;
            if ($user->subscription('main')->onGracePeriod()) {
                $response['isCancelled'] = true;
            }
        } else {
            $response['hasSubscription'] = false;
        }

        return response()->json($response);
    }

    /**
     * Resume a cancelled subscripton
     */
    public function destroy(Request $request, string $planName = '')
    {
        $user = auth()->user();

        try {
            $user->subscription('main')->resume();
        } catch (\Stripe\Error\Base $e) {
            $message = (new ErrorHandler( $e, 'resuming a cancelled subscription' ))->respond();
            return response()->json(['error' => $message], 422);
        }

        event(new SubscriptionResumed($user));

        if( $request->is(
                '/ajax/resumeandswap-subscription/'. $planName .'/destroy')
            ) {
            return redirect('ajax/user-subscription/'. $planName .'/update');
        }
        return response()->json(['success' => true]);
    }
}