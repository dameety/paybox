<?php

namespace Dameety\Paybox\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use Dameety\Paybox\Models\Errors;
use Dameety\Paybox\Http\Controllers\Controller;

class SubscriptionErrorsController extends Controller
{
    public function destroy($slug)
    {
        Errors::findBySlug($slug)->delete();

        return response()->json(['deleted' => true]);
    }
}