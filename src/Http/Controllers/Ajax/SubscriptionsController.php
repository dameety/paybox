<?php

namespace Dameety\Paybox\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use Dameety\Paybox\StripeGateway;
use Dameety\Paybox\Http\Controllers\Controller;

class SubscriptionsController extends Controller
{

    public function create(Request $request)
    {
        auth()->user()->newSubscription($request->plan_name, $request->plan_id)->create($stripeToken);
        return [
            'status' => 'successful'
        ];
    }

    public function update( Request $request )
    {
       $user->subscription('main')->resume();
    }

    public function destroy($plan)
    {
        //cancel a subscription
        auth()->user->subscription('main')->cancel();
        return [
            'cancelled' => true
        ];
    }
}