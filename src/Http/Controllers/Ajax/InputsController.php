<?php
namespace Dameety\Paybox\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use Dameety\Paybox\Http\Controllers\Controller;

class InputsController extends Controller
{
    public function index()
    {
        return config('paybox.inputs');
    }
}
