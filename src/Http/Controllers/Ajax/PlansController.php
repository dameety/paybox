<?php

namespace Dameety\Paybox\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use Dameety\Paybox\Models\Plan;
use Dameety\Paybox\Http\Controllers\Controller;

class PlansController extends Controller
{
    public function destroy(string $slug)
    {
        Plan::findBySlug($slug)->delete();
        return response()->json(['deleted' => true]);
    }

}