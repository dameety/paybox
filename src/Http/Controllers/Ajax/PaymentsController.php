<?php

namespace Dameety\Paybox\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use Dameety\Paybox\Payment;
use Dameety\Paybox\Exceptions\Handler;
use Dameety\Paybox\Http\Controllers\Controller;
use Dameety\Paybox\Repositories\Logs\LogRepository;
use Dameety\Paybox\Repositories\Pages\PageRepository;

class PaymentsController extends Controller
{
    protected $log;
    protected $page;
    protected $payment;

    public function __construct(PageRepository $page, Payment $payment, LogRepository $log)
    {
        $this->log = $log;
        $this->page = $page;
        $this->payment = $payment;
    }

    public function makeRefund(Request $request)
    {
        $request->validate([
            'charge_id' => 'required|string',
            'amount' => 'numeric|nullable'
        ]);

        $response = $this->payment->refundCharge($request->all());
        if( is_object( $response ) ) {
            return response()->json(
                ['message' => $response->getMessage()
            ], 422);
        }
    }

    public function getRefunds($charge_id)
    {
        $response = $this->payment->getRefunds($charge_id);

        if( is_object( $response ) ) {
            return response()->json([
                'message' => $response->getMessage()
            ], 422);
        }

        return response()->json([
            'refunds' => $response
        ]);
    }

}