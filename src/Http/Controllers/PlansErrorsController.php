<?php
namespace Dameety\Paybox\Http\Controllers;

use Dameety\Paybox\Models\Plan;
use Dameety\Paybox\Models\Errors;
use Dameety\Paybox\Http\Controllers\Controller;


class PlansErrorsController extends Controller
{
    public function index()
    {
        return view('Paybox::plans-errors.index', [
            'errors' => Errors::latest('created_at')->get()
        ]);
    }
}