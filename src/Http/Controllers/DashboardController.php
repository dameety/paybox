<?php

namespace Dameety\Paybox\Http\Controllers;

use Illuminate\Http\Request;
use Dameety\Paybox\Http\Controllers\Controller;
use Dameety\Paybox\Repositories\Pages\PageRepository;

class DashboardController extends Controller
{
    protected $page;

    public function __construct (PageRepository $page)
    {
        $this->page = $page;
    }

    protected function index()
    {
        return view('Paybox::dashboard', [
            'totalSales' => $this->page->stats('revenue'),
            'totalQuantitySold' => $this->page->stats('sales'),
            'totalViews' => $this->page->stats('views')
        ]);
    }
}