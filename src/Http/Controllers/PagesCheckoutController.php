<?php
namespace Dameety\Paybox\Http\Controllers;

use Dameety\Paybox\Http\Controllers\Controller;
use Dameety\Paybox\Repositories\Pages\PageRepository;

class PagesCheckoutController extends Controller
{
    protected $page;

    public function __construct(PageRepository $page)
    {
        $this->page = $page;
    }

    public function show(string $slug)
    {
        $page = $this->page->getBySlug($slug);

        if ($page->status !== 'published') {
            return redirect()->route(
                config('paybox.unpublished')
            );
        }

        $pages = $page->increaseViews()->decode();

        return view('Paybox::checkout.show', [
            'page' => $pages
        ]);
    }
}
