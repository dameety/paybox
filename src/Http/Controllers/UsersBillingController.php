<?php
namespace Dameety\Paybox\Http\Controllers;

use Dameety\Paybox\Http\Controllers\Controller;

class UsersBillingController extends Controller
{
    public function show()
    {
        return view('Paybox::user-billing.show');
    }

}