<?php

namespace Dameety\Paybox\Http\Controllers;

use Illuminate\Http\Request;
use Dameety\Paybox\Http\Controllers\Controller;
use Dameety\Paybox\Repositories\Pages\PageRepository;
use Dameety\Paybox\Repositories\Transactions\TransactionRepository;

class TransactionsController extends Controller
{
    protected $page;
    protected $transaction;

    public function __construct(PageRepository $page, TransactionRepository $transaction)
    {
        $this->page = $page;
        $this->transaction = $transaction;
    }

    public function pageTransactions($slug)
    {
        return view('Paybox::transactions.index', [
            'transactions' => $this->page->getTransactions($slug),
            'page' => $this->page->getBySlug($slug)
        ]);
    }
}