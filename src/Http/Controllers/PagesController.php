<?php

namespace Dameety\Paybox\Http\Controllers;

use Dameety\Paybox\Http\Controllers\Controller;
use Dameety\Paybox\Repositories\Pages\PageRepository;

class PagesController extends Controller
{
    protected $page;

    public function __construct(PageRepository $page)
    {
        $this->page = $page;
    }

    public function index()
    {
        return view('Paybox::pages.index', [
            'pages' => $this->page->getAll()
        ]);
    }

    public function create()
    {
        return view('Paybox::pages.create');
    }

    public function edit(string $slug)
    {
        return view('Paybox::pages.edit', [
            'page' => $this->page->getBySlug($slug)
        ]);
    }
}