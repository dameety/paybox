<?php

namespace Dameety\Paybox\Events;

class SubscriptionResumed
{
    public $user;

    /**
     * Create a new event instance.
     *
     * @param $user
     */
    public function __construct($user)
    {
        $this->user = $user;
    }
}