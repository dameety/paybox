<?php

namespace Dameety\Paybox\Events;

class InvoicePaymentSucceeded
{
    /**
     * @var \Laravel\Cashier\Billable
     */
    public $user;

    /**
     * Create a new event instance.
     *
     * @param \Laravel\Cashier\Billable $user
     * @param \Laravel\Cashier\Invoice  $invoice
     */
    public function __construct($user)
    {
        $this->user    = $user;
    }
}