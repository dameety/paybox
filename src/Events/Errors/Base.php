<?php

namespace Dameety\Paybox\Events\Errors;

abstract class Base
{
    public $response;

    public function __construct ($response)
    {
        $this->response = $response;
    }
}