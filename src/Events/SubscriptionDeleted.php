<?php

namespace Dameety\Paybox\Events;

class SubscriptionDeleted
{
    public $user;

    /**
     * Create a new event instance.
     *
     * @param $user
     */
    public function __construct($user)
    {
        $this->user = $user;
    }
}