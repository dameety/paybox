<?php

namespace Dameety\Paybox\Events;

class SubscriptionCreated
{
    public $user;
    public $planId;

    /**
     * Create a new event instance.
     *
     * @param $user
     */
    public function __construct($user, $planId)
    {
        $this->user = $user;
        $this->planId = $planId;
    }
}