let mix = require('laravel-mix');

mix.js('resources/assets/paybox.js', 'public/js/')
    .setPublicPath('public');
