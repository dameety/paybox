@extends('Paybox::layouts.master')
@section('content')

    <div class="uk-background-muted uk-padding-small">
        <ul class="uk-subnav uk-subnav-divider uk-flex-center uk-margin-small">

            <li class="uk-active">
                <a href="{{ route('plan.index') }}">
                    <span class="icon is-small uk-text-primary">
                        <i class="fa fa-folder"></i>
                    </span>
                    <span class="uk-text-primary">
                        All
                    </span>
                </a>
            </li>

            <li>
                <a href="{{ route('plan.create') }}">
                    <span class="icon is-small">
                        <i class="fa fa-plus"></i>
                    </span>
                    New
                </a>
            </li>

            <li>
                <a href="{{ route('subscription-errors.index') }}">
                    <span class="icon is-small">
                        <i class="fa fa-plus"></i>
                    </span>
                    Errors
                </a>
            </li>

        </ul>
    </div>

    <div class="uk-section">
        <div class="uk-container">

            <table class="uk-table uk-table-middle uk-table-striped uk-box-shadow-large">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th class="uk-text-center">Id</th>
                        <th class="uk-text-center">Amount</th>
                        <th class="uk-text-center">Interval</th>
                        <th class="uk-text-center">Features</th>
                        <th class="uk-text-center">Created On</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($plans as $plan)
                    <tr>
                        <td class="uk-table-link">
                            <a class="uk-link-reset" href="{{ route('plan.subscriptions', ['slug' => $plan->slug]) }}">
                                <div>
                                    {{$plan->name}}
                                    <p class="uk-text-meta uk-text-small uk-margin-remove">
                                        Price: ${{$plan->amount}}
                                    </p>
                                </div>
                            </a>
                        </td> {{--  --}}

                        <td class="uk-text-center">
                                <p class="uk-text-meta uk-text-small">
                                   {{$plan->identifier}}
                                </p>
                        </td> {{--  --}}

                        <td class="uk-text-center">
                            <p class="uk-text-meta uk-text-small">
                                    {{$plan->amount}}
                                </p>
                        </td> {{-- quantity sold --}}

                        <td class="uk-text-center">
                            <p class="uk-text-success">
                                {{$plan->interval}}
                            </p>
                        </td> {{--  --}}

                        <td class="uk-text-center">
                            <p class="uk-text-success">
                                {{$plan->features}}
                            </p>
                        </td> {{-- revenue --}}

                        <td class="uk-text-center">
                            <p class="uk-text-meta uk-text-small">
                                {{$plan->created_at}}
                            </p>
                        </td> {{-- Time --}}

                        <td>
                            <div class="uk-text-center">
                                <a @click.prevent="deletePlan('{{ $plan->slug }}')" class="uk-text-danger">
                                    Delete
                                </a>
                            </div>
                        </td> {{-- options --}}
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>

@endsection