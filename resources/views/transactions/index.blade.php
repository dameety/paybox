@extends('Paybox::layouts.master')
@section('content')

    <div class="uk-background-muted uk-padding-small">
        <ul class="uk-subnav uk-subnav-divider uk-flex-center uk-margin-small">

            <li>
                <a href="{{ URL::previous() }}">
                    <span class="icon is-small">
                        <i class="fa fa-step-backward"></i>
                    </span>
                    Back
                </a>
            </li>

            <li class="uk-active">
                <a href="#">
                    <span class="icon is-small uk-text-primary">
                        <i class="fa fa-edit"></i>
                    </span>
                    <span class="uk-text-primary">
                        transaction for: {{ $page->product_name }}
                    </span>
                </a>
            </li>

        </ul>
    </div>

    <inputs-modal v-if="inputModalState" @close="inputModalState = false"></inputs-modal>
    <refund v-if="refundModalState" @close="refundModalState = false"></refund>

    <div class="uk-section">
        <div class="uk-container">

            <table class="uk-table uk-table-middle uk-table-striped uk-box-shadow-large">
                <thead>
                    <tr>
                        <th class="uk-text-center"></th>
                        <th>Page Stats</th>
                        <th>Details</th>
                        <th class="uk-text-center">Inputs</th>
                        <th class="uk-text-center">Refunds</th>
                        <th class="uk-text-center">Time</th>
                        <th class="uk-text-center">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($transactions as $transaction)
                    <tr>
                        <td>
                            <div class="uk-text-center">
                                @if ( $transaction->status === 'success')
                                    <span class="uk-label uk-label-success">{{ $transaction->status }}</span>
                                @else
                                    <span class="uk-label uk-label-danger">{{ $transaction->status }}</span>
                                @endif
                            </div>
                        </td> {{-- status --}}

                        <td>
                            <div>
                                <p class="uk-text-meta uk-text-small">
                                    Product: {{ $transaction->page['name'] }} <br>
                                    Total Sales: ${{ $transaction->page['total_sale'] }} <br>
                                    Quantity Sold: {{ $transaction->page['quantity_sold'] }}
                                </p>
                            </div>
                        </td> {{-- page stats --}}

                        <td>
                            <div>
                                <p class="uk-text-meta uk-text-small">
                                    Charge id: {{ $transaction->charge_id }} <br>
                                    Amount Paid: {{$transaction->page['price']}} <br>
                                    Registered User: {{ $transaction->user }}
                                </p>
                            </div>
                        </td> {{-- Details --}}

                        <td>
                            <div class="uk-text-center">
                                @if(count($transaction->inputs))
                                    <a @click.prevent="showInputs( {{ json_encode( $transaction )}} )" class="uk-button uk-button-small uk-button-default">
                                        View
                                    </a>
                                @elseif (! count($transaction->inputs) )
                                    <p class="uk-text-meta uk-text-small">
                                        No inputs
                                    </p>
                                @endif
                            </div>
                        </td> {{-- Inputs --}}

                        <td>
                            <div class="uk-text-center">
                                <a class="uk-button uk-button-small uk-button-default"
                                    @click.prevent="refundTransaction( {{ json_encode( $transaction )}} )">
                                    View
                                </a>
                            </div>
                        </td> {{-- Refunds --}}

                        <td>
                            <div class="uk-text-center">
                                <p class="uk-text-meta uk-text-small">
                                    {{ $transaction->created_at }}
                                </p>
                            </div>
                        </td> {{-- Time --}}

                        <td>
                            <div class="uk-text-center">
                                <a class="uk-button uk-button-small uk-button-danger"
                                    @click.prevent="deleteTransaction( {{$transaction->id }} )">
                                    Delete
                                </a>
                            </div>
                        </td> {{-- Options --}}
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>

@endsection