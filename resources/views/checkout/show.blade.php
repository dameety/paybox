@extends('Paybox::layouts.master')
@section('header')
@show

@section('content')
    <div class="uk-section">
        <div class="uk-container">

            <div class="uk-box-shadow-large uk-width-large uk-align-center">
                <div class="uk-card uk-card-default">

                    @if($page->img_url)
                        <div class="uk-card-media-top">
                            <img class="uk-width-1-1 uk-height-medium uk-height-max-medium"
                                src="{{ asset($page->img_url) }}" alt="{{$page->product_name}}">
                        </div>
                    @endif

                    <div class="uk-card-body">
                        <h3 class="uk-card-title uk-text-bold uk-margin-remove">
                            {{$page->product_name}}
                        </h3>

                        <p class="uk-margin-remove uk-text-muted uk-text-bold">
                            {{$page->product_info}}
                            Guest or checkout
                        </p>

                        <form method="POST" action="{{ route('processCharge') }}">
                            {{ csrf_field() }}
                            <input name="stripe_token" type="hidden"/>
                            <input name="slug" type="hidden" value="{{ $page->slug }}"/>

                            @if(count($page->inputs))
                                @for ($i = 0; $i < count($page->inputs); $i++)

                                    <div class="uk-margin">
                                        <label class="uk-form-label uk-text-muted" for="{{$page->inputs[$i]}}">{{$page->inputs[$i]}}</label>
                                        <div class="uk-form-controls">
                                            <input class="uk-input {{ $errors->has($page->inputs[$i]) ? ' uk-form-danger' : '' }}" type="text"
                                                name="{{$page->inputs[$i]}}" value="{{ old($page->inputs[$i]) }}" required>

                                            @if ($errors->has( $page->inputs[$i] ))
                                                <p class="text-smaller uk-text-danger uk-margin-remove">
                                                    {{ $errors->first( $page->inputs[$i] ) }}
                                                </p>
                                            @endif

                                        </div>
                                    </div>

                                @endfor
                            @endif

                            <hr>
                                <div class="uk-margin">
                                    <div id="card-element"></div>
                                </div>
                            <hr>

                            <div class="uk-align-center uk-margin-remove">
                                <div class="error-toggle uk-text-center uk-text-danger text-smaller"></div>
                                @include('Paybox::partials._session-messages')
                            </div>

                            <br>

                            <div class="uk-align-center rm-top-margin rm-bottom-margin">
                                <button class="uk-button uk-button-primary uk-box-shadow-xlarge uk-width-1-1">
                                    Pay {{config('paybox.currency.symbol')}}{{$page->price}}
                                </button>
                            </div>

                        </form>
                    </div>

                </div>
            </div>

        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('vendor/paybox/js/stripe-config.js') }}"></script>
@endsection