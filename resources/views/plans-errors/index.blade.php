@extends('Paybox::layouts.master')
@section('content')
    <div class="uk-background-muted uk-padding-small">
        <ul class="uk-subnav uk-subnav-divider uk-flex-center uk-margin-small">

            <li class="uk-active">
                <a href="{{ route('plan.index') }}">
                    <span class="icon is-small">
                        <i class="fa fa-folder"></i>
                    </span>
                    All
                </a>
            </li>

            <li>
                <a href="{{ route('plan.create') }}">
                    <span class="icon is-small">
                        <i class="fa fa-plus"></i>
                    </span>
                    New
                </a>
            </li>

            <li>
                <a href="{{ route('subscription-errors.index') }}">
                    <span class="icon is-small uk-text-primary">
                        <i class="fa fa-folder"></i>
                    </span>
                    <span class="uk-text-primary">
                        Errors
                    </span>
                </a>
            </li>

        </ul>
    </div>

    <div class="uk-section">
        <div class="uk-container">

            <table class="uk-table uk-table-middle uk-table-striped uk-box-shadow-large">
                <thead>
                    <tr>
                        <th>Exception</th>
                        <th class="uk-text-center">user</th>
                        <th class="uk-text-center">action</th>
                        <th class="uk-text-center">Created On</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($errors as $error)
                    <tr>
                        <td class="uk-table-link">
                            <div>
                                {{$error->name}}
                                <p class="uk-text-meta uk-text-small uk-margin-remove">
                                    Price: ${{$error->message}}
                                </p>
                            </div>
                        </td> {{--  --}}

                        <td class="uk-text-center">
                            <p class="uk-text-meta uk-text-small">
                                {{$error->user}}
                            </p>
                        </td> {{-- user --}}

                        <td class="uk-text-center">
                            <p class="uk-text-meta uk-text-small">
                                {{$error->action}}
                            </p>
                        </td> {{-- action --}}

                        <td class="uk-text-center">
                            <p class="uk-text-meta uk-text-small">
                                {{$error->created_at}}
                            </p>
                        </td> {{-- created_at --}}

                        <td>
                            <div class="uk-text-center">
                                <a @click.prevent="deleteError('{{ $error->slug }}')" class="uk-text-danger">
                                    Delete
                                </a>
                            </div>
                        </td> {{-- options --}}
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
@endsection