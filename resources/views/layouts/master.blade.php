<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" href="img/favicon.png">
        <title>{{ config('app.name') }}</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="{{ asset('vendor/paybox/css/uikit.min.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/paybox/css/dashboard.css') }}">
    </head>

    <body>

        <div id="app">

            <div id="barba-wrapper">
                <div class="barba-container">
                    @section('header')
                    @include('Paybox::partials._header')
                    @show

                    @yield('content')
                </div>
            </div>

        </div>

        <script src="https://js.stripe.com/v3/"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/barba.js/1.0.0/barba.min.js"></script>
        <script src="{{ asset('vendor/paybox/js/uikit.min.js') }}"></script>
        <script src="{{ asset('vendor/paybox/js/paybox.js') }}"></script>
        @yield('scripts')
        <script>
            Barba.Pjax.start();
        </script>
    </body>

</html>
