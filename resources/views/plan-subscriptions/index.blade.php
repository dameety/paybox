@extends('Paybox::layouts.master')
@section('content')
    <div class="uk-background-muted uk-padding-small">
        <ul class="uk-subnav uk-subnav-divider uk-flex-center uk-margin-small">

            <li>
                <a href="{{ URL::previous() }}">
                    <span class="icon is-small">
                        <i class="fa fa-step-backward"></i>
                    </span>
                    Back
                </a>
            </li>

            <li class="uk-active">
                <a href="{{ route('plan.index') }}">
                    <span class="icon is-small uk-text-primary">
                        <i class="fa fa-folder"></i>
                    </span>
                    <span class="uk-text-primary">
                        Showing Subscriptons for: {{ $plan->name }}
                    </span>
                </a>
            </li>

            <li>
                <a href="#">
                    Total: {{$total}}
                </a>
            </li>

        </ul>
    </div>
    {{$subscriptions}}
    <div class="uk-section">
        <div class="uk-container">

            <table class="uk-table uk-table-middle uk-table-striped uk-box-shadow-large">
                <thead>
                    <tr>
                        <!-- <th>Name</th> -->
                        <th class="uk-text-center">user_id</th>
                        <th class="uk-text-center">stripe_id</th>
                        <th class="uk-text-center">quantity</th>
                        <th class="uk-text-center">Created On</th>
                        <th class="uk-text-center">Ends On</th>
                        <th class="uk-text-center">trial_ends_at</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($subscriptions as $sub)
                    <tr>
                        <!-- <td>
                            <div>
                                <a href="#" class="uk-text-primary uk-text-capitalize">
                                    Name: {{$sub->name}}
                                </a>
                                <p class="uk-text-meta uk-text-small uk-margin-remove">
                                    Plan Id: {{$sub->stripe_plan}}
                                </p>
                            </div>
                        </td> -->

                        <td class="uk-text-center">
                            <p class="uk-text-meta uk-text-small">
                                {{ $sub->user_id }}
                            </p>
                        </td> {{--  --}}

                        <td class="uk-text-center">
                            <p class="uk-text-meta uk-text-small">
                                {{ $sub->stripe_id }}
                            </p>
                        </td> {{-- stripe_id --}}

                        <td class="uk-text-center">
                            <p class="uk-text-success">
                                {{ $sub->quantity }}
                            </p>
                        </td> {{-- quantity --}}

                        <td class="uk-text-center">
                            <p class="uk-text-meta uk-text-small">
                                {{ $sub->created_at->format('m/d/Y') }}
                            </p>
                        </td> {{-- Time --}}

                        <td class="uk-text-center">
                            <p class="uk-text-success">
                                {{ $sub->ends_at }}
                            </p>
                        </td> {{--  --}}

                        <td class="uk-text-center">
                            <p class="uk-text-success">
                                {{-- {{ $sub->trial_ends_at->format('m/d/Y') }} --}}
                                {{ $sub->trial_ends_at }}
                            </p>
                        </td> {{--  --}}

                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>

@endsection