@extends('Paybox::layouts.master')
@section('content')

    <div class="uk-background-muted uk-padding-small">
        <ul class="uk-subnav uk-subnav-divider uk-flex-center uk-margin-small">
            <li>
                <a href="{{ route('paybox.dashboard') }}">
                    <span class="icon is-small">
                        <i class="fa fa-cogs"></i>
                    </span>
                    Stats
                </a>
            </li>
            <li class="uk-active">
                <a href="{{ route('paybox.logs') }}">
                    <span class="icon is-small uk-text-primary">
                        <i class="fa fa-tasks"></i>
                    </span>
                    <span class="uk-text-primary">
                        Logs
                    </span>
                </a>
            </li>
        </ul>
    </div>


    <inputs-modal v-if="inputModalState" @close="inputModalState = false"></inputs-modal>

    <div class="uk-section">
        <div class="uk-container">

            <table class="uk-table uk-table-middle uk-table-striped uk-box-shadow-large">
                <thead>
                    <tr>
                        <th class="uk-text-center">.</th>
                        <th>Error</th>
                        <th class="uk-text-center">Product</th>
                        <th class="uk-text-center">Inputs</th>
                        <th class="uk-text-center">Time</th>
                        <th class="uk-text-center">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($logs as $log)
                    <tr>
                        <td>
                            <div class="uk-text-center">
                                <span class="uk-label uk-label-danger">
                                    {{ $log->status }}
                                </span>
                            </div>
                        </td> {{-- status --}}

                        <td>
                             <div>
                                <p class="uk-text-meta uk-text-small">
                                    <span class="uk-text-primary">
                                        Message: {{ $log->subject }} <br>
                                    </span>
                                    Type: {{ $log->exception['type'] }} <br>
                                    Code: {{ $log->exception['code'] }} <br>
                                    Status: {{ $log->exception['status'] }} <br>
                                </p>
                            </div>
                        </td> {{-- error --}}

                        <td class="uk-text-center">
                            <p class="uk-text-meta uk-text-small uk-text-capitalize">
                                {{ $log->page }}
                            </p>
                        </td> {{-- page --}}

                        <td>
                            <div class="uk-text-center">
                                @if(count($log->inputs))
                                    <a @click.prevent="showInputs( {{ json_encode( $log )}} )"
                                        class="uk-button uk-button-small uk-button-default">
                                        View
                                    </a>
                                @elseif (! count($log->inputs) )
                                    <p class="uk-text-meta uk-text-small">
                                        No inputs
                                    </p>
                                @endif
                            </div>
                        </td> {{-- checkout inputs--}}

                        <td class="uk-text-center">
                            <p class="uk-text-meta uk-text-small">
                                {{$log->created_at}}
                            </p>
                        </td> {{-- Time --}}

                        <td class="uk-text-center">
                            <a @click.prevent="deleteLog('{{ $log->slug }}')" class="uk-text-danger" href="#">Delete</a>
                        </td> {{-- Options --}}
                    </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $logs->links() }}

        </div>
    </div>

@endsection