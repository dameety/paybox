@extends('Paybox::layouts.master')
@section('content')
    <div class="uk-background-muted uk-padding-small">
        <ul class="uk-subnav uk-subnav-divider uk-flex-center uk-margin-small">

            <li class="uk-active">
                <a href="{{ route('paybox.dashboard') }}">
                    <span class="icon is-small uk-text-primary">
                        <i class="fa fa-dashboard"></i>
                    </span>
                    <span class="uk-text-primary">
                        Dashboard
                    </span>
                </a>
            </li>

            <li>
                <a href="{{ route('paybox.logs') }}">
                    <span class="icon is-small">
                        <i class="fa fa-database"></i>
                    </span>
                    Logs
                </a>
            </li>

        </ul>
    </div>

    <div class="uk-section">
        <div class="uk-container">

            <div class="uk-child-width-1-3@s uk-grid-match" uk-grid v-cloak>
                <div>
                    <div class="uk-card uk-card-default uk-card-body">
                        <h3 class="uk-card-title"> {{ $totalViews }}</h3>
                        <p>Total Views</p>
                    </div>
                </div>
                <div>
                    <div class="uk-card uk-card-default uk-card-body">
                        <h3 class="uk-card-title">$ {{ $totalSales }}</h3>
                        <p>Total Revenue</p>
                    </div>
                </div>
                <div>
                    <div class="uk-card uk-card-default uk-card-body">
                        <h3 class="uk-card-title"> {{ $totalQuantitySold }}</h3>
                        <p>Total Products Sold</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection