@extends('Paybox::layouts.master')
@section('content')

    <div class="uk-background-muted uk-padding-small">
        <ul class="uk-subnav uk-subnav-divider uk-flex-center uk-margin-small">
            <li>
                <a href="{{ route('page.index') }}">
                    <span class="icon is-small">
                        <i class="fa fa-folder"></i>
                    </span>
                    All
                </a>
            </li>

            <li class="is-active">
                <a href="{{ route('page.create') }}">
                    <span class="icon is-small uk-text-primary">
                        <i class="fa fa-plus"></i>
                    </span>
                    <span class="uk-text-primary">
                        New
                    </span>
                </a>
            </li>
        </ul>
    </div>


    <new-page></new-page>

@endsection
