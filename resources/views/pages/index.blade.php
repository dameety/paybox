@extends('Paybox::layouts.master')
@section('content')

    <div class="uk-background-muted uk-padding-small">
        <ul class="uk-subnav uk-subnav-divider uk-flex-center uk-margin-small">
            <li class="uk-active">
                <a href="{{ route('page.index') }}">
                    <span class="icon is-small uk-text-primary">
                        <i class="fa fa-folder"></i>
                    </span>
                    <span class="uk-text-primary">
                        All
                    </span>
                </a>
            </li>

            <li>
                <a href="{{ route('page.create') }}">
                    <span class="icon is-small">
                        <i class="fa fa-plus"></i>
                    </span>
                    New
                </a>
            </li>
        </ul>
    </div>


    <div class="uk-section">
        <div class="uk-container">

            <table class="uk-table uk-table-middle uk-table-striped uk-box-shadow-large">
                <thead>
                    <tr>
                        <th></th>
                        <th>Product</th>
                        <th class="uk-text-center">Checkout</th>
                        <th class="uk-text-center">Sales</th>
                        <th class="uk-text-center">Revenue</th>
                        <th class="uk-text-center">Status</th>
                        <th class="uk-text-center">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($pages as $page)
                    <tr>
                        <td>
                            <img class="uk-align-center uk-preserve-width uk-border-round" width="70" src="{{ asset( $page->img_url ) }}">
                        </td> {{-- product image --}}

                        <td>
                            <div>
                                <a href="{{ route('checkout.show', ['slug' => $page->slug]) }}"
                                    target="blank" class="uk-text-primary uk-text-capitalize">
                                    {{$page->product_name}}
                                </a>
                                <p class="uk-text-meta uk-text-small uk-margin-remove">
                                    Price: ${{$page->price}}
                                </p>
                                <p class="uk-text-meta uk-text-small uk-margin-remove">
                                    Views: {{ $page->views }}
                                </p>
                            </div>
                        </td> {{-- product info --}}

                        <td class="uk-text-center">
                            <a href="#modal-center" uk-toggle
                                @click.prevent="getLink('{{ $page->checkout_link }}')">
                                <p class="uk-text-meta uk-text-small">
                                    Get Link
                                </p>
                            </a>
                        </td> {{-- views --}}

                        <td class="uk-text-center">
                            <a href="{{ route('page.transactions', ['slug' => $page->slug]) }}" class="uk-button uk-button-small uk-button-primary">
                                {{$page->quantity_sold}}
                            </a>
                        </td> {{-- quantity sold --}}

                        <td class="uk-text-center">
                            <p class="uk-text-success">
                                ${{$page->total_sale}}
                            </p>
                        </td> {{-- revenue --}}

                        <td class="uk-text-center">
                            <page-status
                                p-status = "{{ $page->status }}"
                                p-slug =  "{{ $page->slug }}"
                            ></page-status>
                        </td> {{-- status --}}

                        <td>
                            <div class="uk-text-center">
                                <button class="uk-button uk-button-default uk-button-small" type="button">
                                    <span class="icon is-small">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </span>
                                </button>
                                <div uk-dropdown="pos: top-justify">
                                    <ul class="uk-nav uk-dropdown-nav">
                                        <li>
                                            <a class="uk-text-danger" @click.prevent="deletePage('{{ $page->slug }}')">
                                                Delete
                                            </a>
                                        </li>
                                        <li class="uk-nav-divider"></li>
                                        <li>
                                            <a href="{{ route('page.edit', ['slug' => $page->slug]) }}">
                                                Edit
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </td> {{-- options --}}
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>


    <div id="modal-center" class="uk-flex-tocp" uk-modal>
        <div class="uk-modal-dialog uk-margin-auto-vertical">
            <div class="uk-modal-body">
                <textarea class="uk-textarea" rows="2" ref="pageCheckOutText">@{{pageCheckOutLink}}</textarea>
            </div>
            <div class="uk-modal-footer uk-background-muted">
                <button class="uk-button uk-button-primary uk-width-1-1 uk-margin-small-bottom"
                    @click.preve="copyLink">
                    Copy Link
                </button>
            </div>
        </div>
    </div> {{-- pagecheckoutlink modal --}}

@endsection