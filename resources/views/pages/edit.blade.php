@extends('Paybox::layouts.master')
@section('content')

    <div class="uk-background-muted uk-padding-small">
        <ul class="uk-subnav uk-subnav-divider uk-flex-center uk-margin-small">
            <li>
                <a href="{{ URL::previous() }}">
                    <span class="icon is-small">
                        <i class="fa fa-step-backward"></i>
                    </span>
                    Back
                </a>
            </li>

            <li class="is-active">
                <a href="#">
                    <span class="icon is-small uk-text-primary">
                        <i class="fa fa-plus"></i>
                    </span>
                    <span class="uk-text-primary">
                        Editing: {{$page->product_name}}
                    </span>
                </a>
            </li>
        </ul>
    </div>

    <edit-page
        page-slug = "{{ $page->slug }}"
        page-image = {{ asset( $page->img_url ) }}
    ></edit-page>

@endsection
