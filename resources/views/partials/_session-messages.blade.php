@if (session('cardError'))
     <p class="uk-text-center text-smaller uk-text-danger uk-margin-remove">
        {{ session('cardError') }}
    </p>
@endif

@if (session('invalidRequest'))
    <p class="uk-text-center text-smaller uk-text-danger uk-margin-remove">
        {{ session('invalidRequest') }}
    </p>
@endif

@if (session('paymentError'))
    <p class="uk-text-center text-smaller uk-text-danger uk-margin-remove">
        {{ session('paymentError') }}
    </p>
@endif

@if (session('apiConnectionError'))
    <p class="uk-text-center text-smaller uk-text-danger uk-margin-remove">
        {{ session('apiConnectionError') }}
    </p>
@endif

@if (session('generalError'))
    <p class="uk-text-center text-smaller uk-text-danger uk-margin-remove">
        {{ session('generalError') }}
    </p>
@endif

@if (session('createPlan'))
    <div class="uk-width-xlarge uk-align-center uk-text-center successfullySubscribed uk-padding">
        <p class="uk-text-center uk-margin-remove">
            {{ session('createPlan') }}
        </p>
    </div>
@endif