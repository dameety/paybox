    <nav id="nav" class="uk-navbar-container uk-light" uk-navbar>
        <div class="uk-navbar-center">

            <div class="uk-navbar-center-left">
                <div>
                    <ul class="uk-navbar-nav">
                        <li class="{{ Menu::areActiveRoutes(['paybox.dashboard', 'log.index']) }}">
                            <a href="{{ route('paybox.dashboard') }}">
                                Dashboard
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <a class="uk-navbar-item uk-logo"></a>

            <div class="uk-navbar-center-right">
                <div>
                    <ul class="uk-navbar-nav">
                        <li class="{{ Menu::areActiveRoutes(['page.index',
                            'page.new', 'page.edit', 'page.transactions']) }}">
                            <a href="{{ route('page.index') }}">
                                Pages
                            </a>
                        </li>
                    </ul>
                    <ul class="uk-navbar-nav">
                        <li class="{{ Menu::areActiveRoutes(['plan.index',
                            'plan.create', 'plan.store']) }}">
                            <a href="{{ route('plan.index') }}">
                                Plans
                            </a>
                        </li>
                    </ul>
                </div>
            </div>


        </div>
    </nav>
