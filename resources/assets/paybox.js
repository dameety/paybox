require('./components');

const app = new Vue({
    el: '#app',

    data () {
        return {
            transaction: [],
            productName: {},
            transaction: {},
            inputModalState: false,
            refundModalState: false,
            transactionId: '',
            pageCheckOutLink: '',
        }
    },

    methods: {

        deletePage(slug) {
            const vm = this;
            this.$swal({
                title: 'Are you sure?',
                text: 'This action cannot be reversed.',
                type: 'warning',
                showCancelButton: true
            }).then((response) => {
                vm.$http.delete(`/ajax/page/${slug}/delete/`).then((response) => {
                    location.reload();
                });
            });
        },

        deleteLog(slug) {
            const vm = this;
            this.$swal({
                title: 'Are you sure?',
                text: 'THis action cannot be reversed.',
                type: 'warning',
                showCancelButton: true,
            }).then((response) => {
                vm.$http.delete(`/ajax/log/${slug}/delete`).then((response) => {
                    location.reload();
                });
            });
        },

        deleteError(slug) {
            const vm = this;
            this.$swal({
                title: 'Are you sure?',
                text: 'THis action cannot be reversed.',
                type: 'warning',
                showCancelButton: true,
            }).then((response) => {
                vm.$http.delete(`/ajax/error/${slug}/delete`).then((response) => {
                    location.reload();
                });
            });
        },

        deleteTransaction (id) {
            const vm = this;
            this.$swal({
                title: 'Are you sure?',
                text: 'This action cannot be reverted',
                type: 'warning',
                showCancelButton: true,
            }).then((response) => {
                vm.$http.delete(`/ajax/transaction/${id}/delete`).then((response) => {
                    location.reload();
                });
            });
        },

        deletePlan (slug) {
            const vm = this;
            this.$swal({
                title: 'Are you sure?',
                text: 'This action cannot be reverted',
                type: 'warning',
                showCancelButton: true,
            }).then((response) => {
                vm.$http.delete(`/ajax/plan/${slug}/delete`).then((response) => {
                    location.reload();
                });
            });
        },

        refundTransaction (transaction) {
            this.transaction = transaction
            this.refundModalState = true
        },

        showInputs(inputs ) {
            this.transaction = inputs;
            this.inputModalState = true;
        },

        copyLink () {
            const text = this.$refs.pageCheckOutText;
            text.select();
            document.execCommand("Copy");
        },

        getLink (link) {
            this.pageCheckOutLink = link;
        }

    }
})