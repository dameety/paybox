window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');
if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found');
}


import VueSweetAlert from 'vue-sweetalert';

window.Vue = require('vue');
Vue.use(VueSweetAlert);
Vue.prototype.$http = axios;

Vue.component('refund', require('./components/refund.vue'));
Vue.component('new-page', require('./components/new-page.vue'));
Vue.component('edit-page', require('./components/edit-page.vue'));
Vue.component('page-status', require('./components/page-status.vue'));

Vue.component('payment-info', require('./components/payment-info.vue'));
Vue.component('subscribe-action', require('./components/subscribe-action.vue'));

Vue.component('subscription-cancel', require('./components/subscription-cancel.vue'));

Vue.component('inputs-modal', require('./components/inputs-modal.vue'));

window.payboxEvent = new Vue();